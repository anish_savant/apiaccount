package io.javaaccount.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountApiApp {
    public static void main(String args[]){
        SpringApplication.run(AccountApiApp.class,args);
    }
}
