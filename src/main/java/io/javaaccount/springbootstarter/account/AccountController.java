package io.javaaccount.springbootstarter.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping("/accounts")
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @RequestMapping("/accounts/{accountId}")
    public Account getAccount(@PathVariable String accountId) {
        return accountService.getAccount(accountId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/accounts")
    public void addAccount(@RequestBody Account account) {
        accountService.addAccount(account);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/accounts/{accountId}")
    public void addAccount(@RequestBody Account account, @PathVariable String accountId) {
        accountService.updateAccount(accountId, account);
    }
    @RequestMapping(method = RequestMethod.DELETE, value = "/topics/{accountId}")
    public void deleteAccount(@PathVariable String accountId) {
        accountService.deleteAccount(accountId);

    }

}
