package io.javaaccount.springbootstarter.account;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account {

    @Id
    private  String accountId;
    private  String owner;
    private  String accountType;

    public Account(String accountId, String owner, String accountType) {
        this.accountId = accountId;
        this.owner = owner;
        this.accountType = accountType;
    }

    public Account() {
        this.accountId = accountId;
        this.owner = owner;
        this.accountType = accountType;
    }


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }


}
