package io.javaaccount.springbootstarter.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public List<Account> getAllAccounts(){
        //return topics;
        List<Account> accounts =new ArrayList<>();
        accountRepository.findAll().forEach(accounts::add);
        return accounts;
    }

    public Account getAccount(String accountId){
        return accountRepository.findById(accountId).orElse(null);
    }

    public void addAccount(Account account) {
        accountRepository.save(account);
    }

    public void updateAccount(String accountId, Account account) {
        accountRepository.save(account);
    }

    public void deleteAccount(String accountId) {
        accountRepository.deleteById(accountId);

    }
}
